﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Net.Http;

namespace np_lesson6
{
    class Program
    {
        static void Main(string[] args)
        {
            
            HttpWebRequest request = WebRequest.CreateHttp("http://mail.ru");
            request.KeepAlive = true;
            request.UserAgent = "Internet Explorer";
            request.Headers.Add(HttpRequestHeader.Translate, "en");
            request.Method = "GET";
            request.Method = WebRequestMethods.Http.Post;
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            for (int i = 0; i < response.Headers.Count; i++)
            {
                Console.Write(response.Headers.Keys[i]+": ");
                foreach (var item in response.Headers.GetValues(i))
                {
                    Console.Write(item + "; ");
                }
                Console.WriteLine("");
                Console.WriteLine("");
            }

            Console.WriteLine("---------------------------------------------------------");
            StreamReader httpPage = new StreamReader(response.GetResponseStream());
            //Console.WriteLine(httpPage.ReadToEnd());

            Console.ReadLine();
        }
    }
}
